const { configureTnsOAuth, TnsOAuthClient } = require("nativescript-oauth2");
const {
    TnsOaProvider,
    TnsOaProviderOptionsGoogle,
    TnsOaProviderGoogle,
    TnsOaProviderOptionsFacebook,
    TnsOaProviderFacebook,
} = require("nativescript-oauth2/providers");

let client = null;

exports.configureOAuthProviders = () => {
    const googleProvider = configureOAuthProviderGoogle();
    const facebookProvider = configureOAuthProviderFacebook();
    configureTnsOAuth([googleProvider,facebookProvider]);
};

function configureOAuthProviderGoogle() {
    const googleProviderOptions = {
        openIdSupport: "oid-full",
        clientId:
            "196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth.apps.googleusercontent.com",
        redirectUri:
            "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth:/auth",
        urlScheme:
            "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth",
        scopes: ["email"],
    };
    const googleProvider = new TnsOaProviderGoogle(googleProviderOptions);
    return googleProvider;
}

function configureOAuthProviderFacebook() {
    const facebookProviderOptions = {
        openIdSupport: "oid-none",
        clientId: "877532503002711",
        clientSecret: "2e969e6f9036555e114fc9225df55cdb",
        redirectUri: "https://www.facebook.com/connect/login_success.html",
        scopes: ["email"],
    };
    const facebookProvider = new TnsOaProviderFacebook(facebookProviderOptions);
    return facebookProvider;
}

exports.tnsOauthLogin = (providerType) => {
    client = new TnsOAuthClient(providerType);
    client.loginWithCompletion((token, err) => {
        if (err) {
            console.error("loging in somthings went wrong!");
            console.error(err);
        } else {
            console.log(token);
        }
    });
};
